#include <iostream>
#include "ast.hpp"
#include "symtab.hpp"
#include "primitive.hpp"
#include "classhierarchy.hpp"
#include "assert.h"
#include <typeinfo>
#include <stdio.h>

/***********

    Typechecking has some code already provided, you need to fill in the blanks. You should only add code where WRITEME labels
    are inserted. You can just search for WRITEME to find all the instances.
    
    You can find descriptions of every type condition that you must check for in the project webpage. Each condition has its own
    error condition that should be thrown when it fails. Every error condition listed there and also in the "errortype" enum in
    this file must be used somewhere in your code.
    
    Be careful when throwing errors - always throw an error of the right type, using the m_attribute of the node you're visiting
    when performing the check. Sometimes you'll see errors being thrown at strange line numbers; that is okay, don't let that
    bother you aslong as you follow the above principle.

*****/

class Typecheck : public Visitor {
    private:
    FILE* m_errorfile;
    SymTab* m_symboltable;
    ClassTable* m_classtable;
    
    const char * bt_to_string(Basetype bt) {
        switch (bt) {
            case bt_undef:    return "bt_undef";
            case bt_integer:  return "bt_integer";
            case bt_boolean:  return "bt_boolean";
            case bt_function: return "bt_function";
            case bt_object:   return "bt_object";
            case bt_nothing:  return "bt_nothing";
            default:
                              return "unknown";
        }
    }
    
    // the set of recognized errors
    enum errortype 
    {
        no_program,
        no_start,
        start_args_err,
        
        dup_ident_name,
        sym_name_undef,
        sym_type_mismatch,
        call_narg_mismatch,
        call_args_mismatch,
        ret_type_mismatch,
        
        incompat_assign,
        if_pred_err,
        
        expr_type_err,
        
        no_class_method,
    };
    
    // Throw errors using this method
    void t_error( errortype e, Attribute a ) 
    {
        fprintf(m_errorfile,"on line number %d, ", a.lineno );
        
        switch( e ) {
            case no_program: fprintf(m_errorfile,"error: no Program class\n"); break;
            //yes
            case no_start: fprintf(m_errorfile,"error: no start function in Program class\n"); break;
            //yes
            case start_args_err: fprintf(m_errorfile,"error: start function has arguments\n"); break;
            //yes
            case dup_ident_name: fprintf(m_errorfile,"error: duplicate identifier name in same scope\n"); break;
            //all the time
            case sym_name_undef: fprintf(m_errorfile,"error: symbol by name undefined\n"); break;
            //all the time
            case sym_type_mismatch: fprintf(m_errorfile,"error: symbol by name defined, but of unexpected type\n"); break;
            //yes kinda? i mean this never happens in assignment...
            case call_narg_mismatch: fprintf(m_errorfile,"error: function call has different number of args than the declaration\n"); break;
            //yes
            case call_args_mismatch: fprintf(m_errorfile,"error: type mismatch in function call args\n"); break;
            //yes
            case ret_type_mismatch: fprintf(m_errorfile, "error: type mismatch in return statement\n"); break;
            //yes
            case incompat_assign: fprintf(m_errorfile,"error: types of right and left hand side do not match in assignment\n"); break;
            //yes
            case if_pred_err: fprintf(m_errorfile,"error: predicate of if statement is not boolean\n"); break;
            //yes
            case expr_type_err: fprintf(m_errorfile,"error: incompatible types used in expression\n"); break;
            //yes
            case no_class_method: fprintf(m_errorfile,"error: function doesn't exist in object\n"); break;
            //yes
            
            default: fprintf(m_errorfile,"error: no good reason\n"); break;
        }
        exit(1);
    }
    
    public:
    
    Typecheck(FILE* errorfile, SymTab* symboltable) {
        m_errorfile = errorfile;
        m_symboltable = symboltable;
        m_classtable = new ClassTable();
    }
    ~Typecheck() {
        delete m_classtable;
    }
    void visitProgramImpl(ProgramImpl *p) {
      
     //WRITE ME
      p->visit_children(this);
  
      const char * shouldbeprog = ((ClassImpl*) p->m_class_list->back())->m_attribute.m_type.classType.classID;
      if(strcmp(shouldbeprog, "Program")){
        t_error(no_program,p->m_attribute);
      }


    }
    
    void visitClassImpl(ClassImpl *p) {
    
      //WRITE ME
      
      //check if classname exists already, it shouldnt
      char * classname = strdup(((ClassIDImpl *) p->m_classid_1)->m_classname->spelling() );
      
      m_symboltable->open_scope();
      if(m_classtable->exist(classname))
      {
        t_error(dup_ident_name, p->m_attribute);
      }
      //check if cfrom name exists already, it should
      char* superclassname = NULL ;
      if(p->m_classid_2 != NULL) {
      superclassname = strdup(((ClassIDImpl *) p->m_classid_2)->m_classname->spelling() );
      if(!m_classtable->exist(superclassname)){
        //cout<<
        //cout<<"error is here"<<endl;
        t_error(sym_name_undef, p->m_attribute);
      }

      //look at classhierarchy
      //insert shit
      //look up the trees
      //etc
      

      
      SymScope * classcope = m_symboltable->get_current_scope();
      m_classtable->insert(classname, superclassname, p, classcope);
      //cout<<"class name is "<< classname <<" super class is " << superclassname << " scope is "<< &classcope << endl;

      }

      SymScope * classcope = m_symboltable->get_current_scope();
      m_classtable->insert(classname, superclassname, p, classcope);  
      //cout<<"class name is "<< classname <<" super class is " << superclassname << " scope is "<< &classcope << endl;


      p->m_attribute.m_type.classType.classID = classname;//p->m_parent



      p->visit_children(this);

      //cout<<"is error here?"<<endl;
      //check if we made a method for start in program
      if(!strcmp(classname,"Program")){
        //we're in a program class
        const char * start = "start";
        if(!m_symboltable->exist(strdup(start))){
          t_error(no_start, p->m_attribute);
        }
      }
      m_symboltable->close_scope();
      //cout<<"sdgsdg?"<<endl;


    }
    
    void visitDeclarationImpl(DeclarationImpl *p) {
      
      //WRITE ME
      //m_symboltable->insert(strdup(bt_to_string(p->m_attribute->m_type.baseType)), &p->m_attribute->m_type );
      //p->m_variableid_list
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      p->visit_children(this);

      if (p->m_variableid_list != NULL) {
      list<VariableID_ptr>::iterator m_variableid_list_iter;
      for(m_variableid_list_iter = p->m_variableid_list->begin();
      m_variableid_list_iter != p->m_variableid_list->end();
      ++m_variableid_list_iter){
      if(*m_variableid_list_iter != NULL) {

        const char * name = strdup(((VariableIDImpl *) *m_variableid_list_iter)->m_symname->spelling() );

        if(m_symboltable->exist(name))
        {
        t_error(dup_ident_name, p->m_attribute);
        }
        //else{
        else{

          //look in all the super scopes for the current class
          const char * classname = p->m_attribute.m_type.classType.classID;
          //if class exists


          if(!m_classtable->exist(strdup(classname))){
            t_error(sym_name_undef, p->m_attribute);
          }
          ClassNode * currclass = m_classtable->lookup(strdup(classname));
          //cout << "first class is " <<currclass->name->spelling()<<endl;
          SymScope * currscope = currclass->scope;
          bool done = false;
          while(!done){
            //cout<<"entering while with "<< currclass->name->spelling()<<endl;
            //cout<<"looking for "<< name << endl;
            //currscope->dump(stderr, 0);
            if(currscope->exist(name)){
              t_error(dup_ident_name, p->m_attribute);
              break;
            }
            else{
    
              if(currclass->superClass){
                //cout<<"can we please"<<endl;
                   break;
              }
              const char* supername = strdup(currclass->superClass->spelling());

              if(!m_classtable->exist(strdup(supername))){
                t_error(sym_name_undef, p->m_attribute);
              }
              //cout<<"where"<<endl;

              currclass = m_classtable->lookup(strdup(supername));
              currscope = currclass->scope;
            }
          } //end while loop
        } // end else

          AllType* a = new AllType(); 
          a = &(p->m_attribute.m_type);
          
          a->classType.classID = p->m_attribute.m_type.classType.classID;
          if(a->baseType & bt_object){
            //cout <<"hersdgsdge"<<endl;
            a->classType.classID = ( (ClassIDImpl *) ((TObject *)p->m_type)->m_classid)->m_classname->spelling();
          //  cout<< name << " " << a->classType.classID << endl;
            //cout<< (p->m_attribute->m_type->m_spelling()) <<endl;
          }
          //cout<< name << " " << a->classType.classID << endl;
          m_symboltable->insert(strdup(name), a);



      } //cif var not
   } //current variable in list
}

}
  

    
    
    void visitMethodImpl(MethodImpl *p) {
    
      //WRITE ME
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      char * methname = strdup(((MethodIDImpl *) p->m_methodid)->m_symname->spelling() );
      //if(m_symboltable->exist(methname))
      //{
      //  t_error(dup_ident_name, p->m_attribute);
     // }

      if(m_symboltable->exist(methname))
        {
        t_error(dup_ident_name, p->m_attribute);
        }
        //else{
        else{

          //look in all the super scopes for the current class
          const char * classname = p->m_attribute.m_type.classType.classID;
          //if class exists


          if(!m_classtable->exist(strdup(classname))){
            t_error(sym_name_undef, p->m_attribute);
          }
          ClassNode * currclass = m_classtable->lookup(strdup(classname));
          //cout << "first class is " <<currclass->name->spelling()<<endl;
          SymScope * currscope = currclass->scope;
          bool done = false;
          while(!done){
            //cout<<"entering while with "<< currclass->name->spelling()<<endl;
            //cout<<"looking for "<< name << endl;
            //currscope->dump(stderr, 0);
            if(currscope->exist(methname)){
              t_error(dup_ident_name, p->m_attribute);
              break;
            }
            else{
    
              if(currclass->superClass){
                //cout<<"can we please"<<endl;
                   break;
              }
              const char* supername = strdup(currclass->superClass->spelling());

              if(!m_classtable->exist(strdup(supername))){
                t_error(sym_name_undef, p->m_attribute);
              }
              //cout<<"where"<<endl;

              currclass = m_classtable->lookup(strdup(supername));
              currscope = currclass->scope;
            }
          } //end while loop
        } // end else

      //check EVERYTHING




      //call accept
      //p->m_methodbody->accept(this);
      //visit kids
      m_symboltable->open_scope();

      //accept on methodname, accept on args, accept on return type FIRST

      p->m_methodid->accept(this);
      
      p->m_type->accept(this);

      AllType* method = new AllType();      
      AllType* idealreturn = new AllType(); 
      idealreturn = &(p->m_attribute.m_type);

      method->classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //cout<< methname << " is from class : " << method->classType.classID << endl;
      //cout << "the return should be " <<( (ClassIDImpl *) ((TObject *)p->m_type)->m_classid)->m_classname->spelling();
      //cout<<"I'm method " << methname << " amd my type is "<< method->classType.classID << endl;
      
      
      //cout <<"in method "<<methname <<" "<< idealreturn->classType.classID << endl;
      method->methodType.returnType.baseType = idealreturn->baseType;
      //cout <<"where it at" <<bt_to_string(method->methodType.returnType.baseType) << endl;

      //check if its a start method
      
      const char * start = "start";
      if(!strcmp(start,methname)){

        if (p->m_parameter_list->size() != 0){
          t_error(start_args_err, p->m_attribute);
        }
        
      }
   
      if (p->m_parameter_list != NULL) {
        list<Parameter_ptr>::iterator m_parameter_list_iter;
        int i = 0;
        for(m_parameter_list_iter = p->m_parameter_list->begin();
        m_parameter_list_iter != p->m_parameter_list->end();
        ++m_parameter_list_iter){
          i++;
        if(*m_parameter_list_iter != NULL) {
          (*m_parameter_list_iter)->accept(this);
            CompoundType* parameter = new CompoundType();
            
            parameter->baseType = ((ParameterImpl *) *m_parameter_list_iter)->m_attribute.m_type.baseType;
            method->methodType.argsType.push_back(*parameter);

            //method->methodType.argsType[i].baseType = parameter->baseType;
            
            //cout<< bt_to_string(parameter->baseType)<< endl;
          }
        }
      }
      //cout <<"where it at" <<bt_to_string(method->methodType.returnType.baseType) << endl;
      p->m_attribute.m_type.baseType = method->methodType.returnType.baseType;
      SymScope * s = (m_symboltable->get_current_scope());
      //cout<< methname <<" "<< &s<<endl;
      m_symboltable->insert_in_parent_scope(methname, method);


      //check for return type of it
      p->m_methodbody->accept(this);
       

      AllType* actualreturn = new AllType();
      //cout<<"fault here"<<endl;
      actualreturn = &(((ReturnImpl *) ((MethodBodyImpl *)p->m_methodbody)->m_return)->m_expression->m_attribute.m_type);

      //cout<<"fault here"<<endl;

      //cout << bt_to_string(actualreturn->baseType) << endl;
      //cout<<"fault here"<<endl;
      //cout << bt_to_string(idealreturn->baseType) << endl;
      if (!(actualreturn->baseType & idealreturn->baseType)) {

        t_error(ret_type_mismatch, p->m_attribute);
      }

      // if both return types are bt object 
      // check if they're the same class by class name
      // then set it
      else if(actualreturn->baseType == bt_object) {

        //recurse through their class trees to see if they are the same class type
        if(strdup(actualreturn->classType.classID) != strdup(idealreturn->classType.classID)){
            //check the parent classes
          //cout<<"segging"<<endl;

          //cout<<actualreturn->classType.classID <<" and " << idealreturn->classType.classID <<endl;

          if(actualreturn->classType.classID != idealreturn->classType.classID){
            //check the parent classes
          //cout<<"segging"<<endl;

          const char* nametomatch = actualreturn->classType.classID;
          //cout<<idealreturn->classType.classID<<endl;

            if(!m_classtable->exist(strdup(idealreturn->classType.classID))){
              t_error(sym_name_undef, p->m_attribute);
            }
            bool done = false;
            //start iterative code to find superclass
            ClassNode* currclass = m_classtable->lookup(strdup(idealreturn->classType.classID));
            while(!done){
              //cout<<"now im in " <<currclass->name->spelling() <<endl;
              if(currclass->superClass->spelling() == NULL){
                t_error(ret_type_mismatch, p->m_attribute);
              }
              const char * supername = currclass->superClass->spelling();
              if (!strcmp(supername,nametomatch)){
                done = true;
                break;
              }
              if(!m_classtable->exist(strdup(supername))){
                t_error(sym_name_undef, p->m_attribute);
              }
              currclass = m_classtable->lookup(strdup(supername));
            }
          } //end if

        } // end real if

      }
      //assignment of method's return type
           // cout<<"segfault"<<endl;



      //get method information and add to symbol table
      //go over each parameter, add parameter to compound type
      
      //if return type is object , get class id add it to it

      //add every argument and their type to the symbol table

      //put entire AllType into symtab as bt_function
      //cout<<"inserting for "<<methname<<endl;
      //cout<<"its type is "<<bt_to_string(method->methodType.returnType.baseType)<<endl;
      //m_symboltable->insert(methname, method);

      //

      m_symboltable->close_scope();


    }
    
    void visitMethodBodyImpl(MethodBodyImpl *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);



    }
    
    void visitParameterImpl(ParameterImpl *p) {
      //p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      const char *name = strdup(((VariableIDImpl *) p->m_variableid)->m_symname->spelling());
      //var shouldnt exist yet
      //open scope?? maybe
      //create new var , insert in table with corresponding type,
      if(m_symboltable->exist(name)){
        t_error(dup_ident_name, p->m_attribute);
      }
      else{
        AllType* a = new AllType(); 
        a = &(p->m_attribute.m_type);

        //check if its a object, then set its class id
        if(p->m_attribute.m_type.baseType == bt_object){


          //insert for specific class, this is the type it should be of 
          a->classType.classID = ( (ClassIDImpl *) ((TObject *)p->m_type)->m_classid)->m_classname->spelling();
          //cout<< "type is " <<a->classType.classID << endl;
          p->m_attribute.m_type.classType.classID = a->classType.classID;

        /*if(actualreturn->classType.classID != idealreturn->classType.classID){
            //check the parent classes
          //cout<<"segging"<<endl;
          const char* nametomatch = actualreturn->classType.classID;
          cout<<actualreturn->classType.classID;
            if(!m_classtable->exist(strdup(actualreturn->classType.classID))){
              t_error(sym_name_undef, p->m_attribute);
            }
            bool done = false;
            //start iterative code to find superclass
            ClassNode* currclass = m_classtable->lookup(strdup(actualreturn->classType.classID));
            while(!done){
              if(currclass->superClass->spelling() == NULL){
                t_error(ret_type_mismatch, p->m_attribute);
              }
              const char * supername = currclass->superClass->spelling();
              if (supername == nametomatch){
                done = true;
                break;
              }
              if(!m_classtable->exist(strdup(supername))){
                t_error(sym_name_undef, p->m_attribute);
              }
              currclass = m_classtable->lookup(strdup(supername));
            }
        } //end if
}
*/



        }

        m_symboltable->insert(strdup(name), a);
      }

    }
    
    void visitAssignment(Assignment *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);


      const char *name = strdup(((VariableIDImpl *) p->m_variableid)->m_symname->spelling());
      if(!(m_symboltable->exist(name))) {

        t_error(sym_name_undef, p->m_attribute);
      }
      else{ 
        Symbol * looked = m_symboltable->lookup(strdup(name));
        
        //cout<< "lhs is " << bt_to_string(looked->baseType) << endl;
        //cout<< "rhs is "<< bt_to_string(p->m_expression->m_attribute.m_type.baseType) << endl;

        if(looked->baseType & (bt_function | bt_undef)){
          //cout<<"wheeere?"<<endl;
          t_error(sym_type_mismatch, p->m_attribute);
        }

        if( !(looked->baseType & p->m_expression->m_attribute.m_type.baseType)) {
        //cout<< bt_to_string(looked->baseType)<<endl;
        //cout<< bt_to_string(p->m_expression->m_attribute.m_type.baseType) << endl;
        t_error(incompat_assign, p->m_attribute);
        }
      }
      
      //ADD THIRD CASE FOR OBJECT AND CLASS INHERITANCE BULLSHIT!!!!!!!!!
    }
    void visitIf(If *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      if(!(p->m_expression->m_attribute.m_type.baseType & bt_boolean)){
        t_error(if_pred_err, p->m_attribute);
      }

    }
    
    void visitPrint(Print *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      
    }
    
    void visitReturnImpl(ReturnImpl *p) {

    p->m_attribute.m_type.classType.classID = strdup(p->m_parent_attribute->m_type.classType.classID);

      //WRITE ME
      p->visit_children(this);
      
      if(p->m_expression->m_attribute.m_type.baseType == bt_object){
        //cout<< "in return impl val is "<< p->m_expression->m_attribute.m_type.classType.classID << endl;
        //cout<<"hey there"<<endl;  
      }

    }
    
    void visitTInteger(TInteger *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      p->m_parent_attribute->m_type.baseType = bt_integer;

    }
    
    void visitTBoolean(TBoolean *p) {
    
      //WRITE ME
      p->visit_children(this);
      p->m_parent_attribute->m_type.baseType = bt_boolean;

    }
    
    void visitTNothing(TNothing *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      p->m_parent_attribute->m_type.baseType = bt_nothing;
    }
    
    void visitTObject(TObject *p) {
      //p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);
      p->m_parent_attribute->m_type.baseType = bt_object;
    }
    
    void visitClassIDImpl(ClassIDImpl *p) {
      //p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
    
      //WRITE ME
      p->visit_children(this);
    }
    
    void visitVariableIDImpl(VariableIDImpl *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);

    }
    
    void visitMethodIDImpl(MethodIDImpl *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);

    }
    
    void visitPlus(Plus *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //if( p->m_expression_1->m_attribute & bt_integer)
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {

        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_integer;
      }
      //WRITE ME
    }
    
    void visitMinus(Minus *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {

        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_integer;
      }
    }
    
    void visitTimes(Times *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {

        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_integer;
      }

    }
    
    void visitDivide(Divide *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {
        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_integer;
      }

    }

    
    void visitAnd(And *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_boolean) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_boolean))) {
        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_boolean;
      }
    }
    
    void visitLessThan(LessThan *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
       if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {

        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_boolean;
      }
    }
    
    void visitLessThanEqualTo(LessThanEqualTo *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression_1->m_attribute.m_type.baseType == bt_integer) 
        && (p->m_expression_2->m_attribute.m_type.baseType == bt_integer))) {

        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_boolean;
      }
    }
    
    void visitNot(Not *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression->m_attribute.m_type.baseType == bt_boolean))) {
        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_boolean;
      }
    }
    
    void visitUnaryMinus(UnaryMinus *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      if(!((p->m_expression->m_attribute.m_type.baseType == bt_integer))) {
        t_error(expr_type_err, p->m_attribute);
      }
      else{
      p->m_attribute.m_type.baseType = bt_boolean;
      }
    }
    
    void visitMethodCall(MethodCall *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);

      //method should check its current scope
      //then check its parents scope
      //iteratively check all parents scope for method
      char* varname = strdup(((VariableIDImpl *) p->m_variableid)->m_symname->spelling());
      char* methname = strdup(((MethodIDImpl *) p->m_methodid)->m_symname->spelling());

      //cout<<"here with "<< varname <<" "<< methname <<endl;
      if(!m_symboltable->exist(varname)){

        t_error(sym_name_undef,p->m_attribute);
      }
      //the object is of this type
      Symbol* object = m_symboltable->lookup(strdup(varname));

      //check its actually an object
      if(!(object->baseType & bt_object)){
        t_error(sym_type_mismatch, p->m_attribute);
      }
      //get its classname
      const char* classname = object->classType.classID;
      //check that its actually in the class table
      if(!m_classtable->exist(strdup(classname))) {
        //cout<<"it exists" << endl;
        t_error(sym_name_undef,p->m_attribute);

      }

      ClassNode * node = m_classtable->lookup(strdup(classname));
      bool done = false;
      while(!done)
      {
        //cout<<"we're in this class: "<<node->name->spelling() << endl;
        SymScope* currentscope = node->scope;
        if(currentscope->exist(strdup(methname))) {
          done = true;
          //this is the method that we want it to look like
          
          Symbol * callermethod = currentscope->lookup(methname);

          //the thing thats trying to call method is using the expression list to insert args
          if (p->m_expression_list != NULL) {
            
            //check if number of args match
            if(!(p->m_expression_list->size() == callermethod->methodType.argsType.size())){
              t_error(call_narg_mismatch, p->m_attribute);
            }
            //cout<<"i'm here"<<endl;
            int i = 0;

            list<Expression_ptr>::iterator m_expression_list_iter;
            for(m_expression_list_iter = p->m_expression_list->begin();m_expression_list_iter != p->m_expression_list->end();++m_expression_list_iter){
              if(*m_expression_list_iter != NULL) {
               
              //cout<<bt_to_string(callermethod->methodType.argsType[i].baseType) <<" and " << bt_to_string((*m_expression_list_iter)->m_attribute.m_type.baseType)<< endl;

                //check if each arguments match type wise
                if(!(callermethod->methodType.argsType[i].baseType & (*m_expression_list_iter)->m_attribute.m_type.baseType)){
                  t_error(call_args_mismatch, p->m_attribute);
                }
              }
               i++;
            } // end for loop
          } //end thing saying if the expression list is not null

          //cout<< bt_to_string(callermethod->methodType.returnType.baseType)<<endl;
          p->m_attribute.m_type.baseType = callermethod->methodType.returnType.baseType;
        }
        else{
          //it wasn't in our scope, so we check node's parent's scope.
          if(node->superClass->spelling() == NULL){
            //there's no super class and method isnt in subclasses. no method.
            //cout << "are we in here?"<<endl;
            t_error(no_class_method,p->m_attribute);
          }
          //cout<<"method not found in nodes scope"<<endl;

          char * superclassname = strdup(node->superClass->spelling());
          //cout<<superclassname<<endl;

          node = m_classtable->lookup(superclassname);
          //the new scope to look in

        }
      }
      
      //if the calledmethod exists in current scope
      //cout << classname <<endl;

    //cout<<"here"<< methname<<endl;


      /*if(m_symboltable->exist(strdup(methname))){ //method is in our current scope
        cout<<"help"<<endl;
        Symbol * calledmethod = m_symboltable->lookup(strdup(methname));
        //const char * classname = calledmethod->classType.classID;
        if(!m_classtable->exist(strdup(classname))) {
        //cout<<"where";
            t_error(sym_name_undef,p->m_attribute);
          }

        SymScope* currentscope = m_symboltable->get_current_scope();
      if(currentscope->exist(methname)){
        cout<<"here ever?"<<endl;
        //check if the return type is the same
        Symbol * callermethod = currentscope->lookup(methname);
        Basetype calledreturn = calledmethod->methodType.returnType.baseType;
        Basetype callerreturn = callermethod->methodType.returnType.baseType;
        if(!(callerreturn & calledreturn)){
          t_error(ret_type_mismatch, p->m_attribute);
        }
        //check if the parameters match up with the arguments and check if the # of args is same
        std::vector<CompoundType>argslist = calledmethod->methodType.argsType;
        std::vector<CompoundType>argslist2 = callermethod->methodType.argsType;

        int i = 0;
        if(argslist.size() != argslist2.size()){
          t_error(call_narg_mismatch, p->m_attribute);
        }

        for(i = 0; i < argslist.size(); ++i){
            if (!(argslist[i].baseType & argslist2[i].baseType)){
              t_error(call_args_mismatch, p->m_attribute);
            }
        }
        p->m_attribute.m_type.baseType = calledreturn; 
      }

    }
    else {
        //cout<<methname<<endl;
        // else were in a parent class now
        //cout <<" getting here" << endl;
        //if(node->superClass != NULL){
        //  cout<< "its not null" <<endl;
        //}
        //cout <<node->superClass->spelling() <<endl;
        char * supername = strdup(node->superClass->spelling());
        //cout<< supername <<endl;
        if(!m_classtable->exist(strdup(supername))){
          cout << "does it exist "<<endl;
        }

        ClassNode* node = m_classtable->lookup(strdup(supername));
        //cout<<"sgsdg"<<endl;  
        bool done = false;
        while(!done){

           char * parentname = strdup(node->name->spelling());
           SymScope* parentscope = node->scope;

           cout<<"entering while loop with "<< parentname<< endl;
           //cout <<parentscope << endl;
           //cout<<"here"<<endl;
           //cout <<methname << endl;

           if(parentscope->exist(methname)){
            done = true;
            if(m_symboltable->exist(strdup(methname))){
              cout << "it exists"<<endl;
            }
            cout<<"it dont "<<endl;
            Symbol* calledmethod = m_symboltable->lookup(strdup(methname));
         //check if the return type is the same
            Symbol * callermethod = parentscope->lookup(methname);

            Basetype calledreturn = calledmethod->methodType.returnType.baseType;
            Basetype callerreturn = callermethod->methodType.returnType.baseType;
            if(!(callerreturn & calledreturn)){
                t_error(ret_type_mismatch, p->m_attribute);
               }
        //check if the parameters match up with the arguments and check if the # of args is same
            std::vector<CompoundType>argslist = calledmethod->methodType.argsType;
            std::vector<CompoundType>argslist2 = callermethod->methodType.argsType;
           cout<<"here"<<endl;

            int i = 0;
            if(argslist.size() != argslist2.size()){
              t_error(call_narg_mismatch, p->m_attribute);
            }

            for(i = 0; i < argslist.size(); ++i){
               if (!(argslist[i].baseType & argslist2[i].baseType)){
                t_error(call_args_mismatch, p->m_attribute);
                }
              }
              cout<<"here ever?"<<endl;
             cout<<bt_to_string(p->m_attribute.m_type.baseType) << endl;
             p->m_attribute.m_type.baseType = calledreturn; 

          }

          //call this in else 
          char * newsupername = strdup(node->superClass->spelling());
          node = m_classtable->lookup(strdup(newsupername));
        } //end whiel looop
      }
    */

    }
    
    void visitSelfCall(SelfCall *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;

      //WRITE ME
      p->visit_children(this);

      //first lookup name variable id
      const char* name = strdup(((MethodIDImpl *) p->m_methodid)->m_symname->spelling());
      //if(!m_symboltable->exist(name)){
      //  t_error(sym_name_undef, p->m_attribute);
      //}

      const char * classname = p->m_attribute.m_type.classType.classID;

      if(m_symboltable->exist(strdup(name))){
      Symbol * themethod = m_symboltable->lookup(strdup(name));
      //foreach thing in expression list do something
      int argslength = themethod->methodType.argsType.size();
      int i = 0;
      std::vector<CompoundType> argslist = themethod->methodType.argsType;

      if (p->m_expression_list != NULL) {
      int calledlength = p->m_expression_list->size();

      //check if argument numbers match
      if(calledlength != argslength){
        t_error(call_narg_mismatch, p->m_attribute);
      }

      //for each parameter, check if args match parameters
      list<Expression_ptr>::iterator m_expression_list_iter;
      for(m_expression_list_iter = p->m_expression_list->begin();m_expression_list_iter != p->m_expression_list->end();++m_expression_list_iter){
        if(*m_expression_list_iter != NULL) {

          //throw error if they dont match
          if(!(argslist[i].baseType == (*m_expression_list_iter)->m_attribute.m_type.baseType)){
            t_error(call_args_mismatch, p->m_attribute);
            }
          }
         i++; 
        }
      }
      p->m_attribute.m_type.baseType = themethod->methodType.returnType.baseType;
    }// if its in current scope
    else {
      //we're in another scope for the method.
       ClassNode * node = m_classtable->lookup(strdup(classname));
      bool done = false;
      while(!done)
      {
        //cout<<"we're in this class: "<<node->name->spelling() << endl;
        SymScope* currentscope = node->scope;
        if(currentscope->exist(strdup(name))) {
          done = true;
          //this is the method that we want it to look like
          
          Symbol * callermethod = currentscope->lookup(name);

          //the thing thats trying to call method is using the expression list to insert args
          if (p->m_expression_list != NULL) {
            
            //check if number of args match
            if(!(p->m_expression_list->size() == callermethod->methodType.argsType.size())){
              t_error(call_narg_mismatch, p->m_attribute);
            }
            //cout<<"i'm here"<<endl;
            int i = 0;

            list<Expression_ptr>::iterator m_expression_list_iter;
            for(m_expression_list_iter = p->m_expression_list->begin();m_expression_list_iter != p->m_expression_list->end();++m_expression_list_iter){
              if(*m_expression_list_iter != NULL) {
               
              //cout<<bt_to_string(callermethod->methodType.argsType[i].baseType) <<" and " << bt_to_string((*m_expression_list_iter)->m_attribute.m_type.baseType)<< endl;

                //check if each arguments match type wise
                if(!(callermethod->methodType.argsType[i].baseType & (*m_expression_list_iter)->m_attribute.m_type.baseType)){
                  t_error(call_args_mismatch, p->m_attribute);
                }
              }
               i++;
            } // end for loop
          } //end thing saying if the expression list is not null

          //cout<< bt_to_string(callermethod->methodType.returnType.baseType)<<endl;
          p->m_attribute.m_type.baseType = callermethod->methodType.returnType.baseType;
        }
        else{
          //it wasn't in our scope, so we check node's parent's scope.
          //cout<<"here?"<<endl;
          //cout<< node->name->spelling() << endl;
          if(node->superClass->spelling() == NULL){
            //there's no super class and method isnt in subclasses. no method.
            //cout << "are we in here?"<<endl;

            t_error(sym_name_undef,p->m_attribute);
          }
          //cout<<"method not found in nodes scope"<<endl;

          char * superclassname = strdup(node->superClass->spelling());
          //cout<<superclassname<<endl;

          node = m_classtable->lookup(superclassname);
          //the new scope to look in

        }
      }


    }

    //p->m_attribute.m_type.baseType = themethod->methodType.returnType.baseType;
    //method->methodType.returnType.baseType

    }
    
    void visitVariable(Variable *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      // pass the info up
      
      const char *name = strdup(((VariableIDImpl *) p->m_variableid)->m_symname->spelling());
      //if(!m_symboltable->exist(name)){

        //t_error(sym_name_undef, p->m_attribute);
      //}
      
        AllType* a = new AllType(); 
        //variable is from our current scope
        if(m_symboltable->exist(name)){
          a = (m_symboltable->lookup(name));
          p->m_attribute.m_type.baseType = a->baseType;
          p->m_attribute.m_type.classType.classID = a->classType.classID;
        }
        else{
          //look in all the super scopes for the current class

          const char * classname = p->m_attribute.m_type.classType.classID;
          //if class exists
          if(!m_classtable->exist(strdup(classname))){
            t_error(sym_name_undef, p->m_attribute);
          }
          ClassNode * currclass = m_classtable->lookup(strdup(classname));
          //cout << "first class is " <<currclass->name->spelling()<<endl;
          SymScope * currscope = currclass->scope;
          bool done = false;
          while(!done){
            //cout<<"entering while with "<< currclass->name->spelling()<<endl;
            //cout<<"looking for "<< name << endl;
            //currscope->dump(stderr, 0);
            if(currscope->exist(name)){
              a = currscope->lookup(name);
              p->m_attribute.m_type.baseType = a->baseType;
              p->m_attribute.m_type.classType.classID = currclass->name->spelling();
              done = true;
              break;
            }
            else{
              //cout <<"entering else"<<endl;
              //cout<<"we get to "<< currclass->superClass->spelling() << endl;
              //if super class exists 
              const char* supername = strdup(currclass->superClass->spelling());
              //cout<<"the soop name is " <<supername << endl;
              if(supername == NULL){
                   t_error(sym_name_undef, p->m_attribute);
              }

              if(!m_classtable->exist(strdup(supername))){
                t_error(sym_name_undef, p->m_attribute);
              }
              currclass = m_classtable->lookup(strdup(supername));
              currscope = currclass->scope;
            }
          }

        }

        //m_symboltable->insert(strdup(name), a);
      

    }
    
    void visitIntegerLiteral(IntegerLiteral *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      p->m_attribute.m_type.baseType = bt_integer;
      p->m_parent_attribute = &p->m_attribute;
      
    }
    
    void visitBooleanLiteral(BooleanLiteral *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      p->visit_children(this);
      p->m_attribute.m_type.baseType = bt_boolean;
      p->m_parent_attribute = &p->m_attribute;

    }
    
    void visitNothing(Nothing *p) {
      p->m_attribute.m_type.classType.classID = p->m_parent_attribute->m_type.classType.classID;
      //WRITE ME
      p->visit_children(this);
      p->m_attribute.m_type.baseType = bt_nothing;
      p->m_parent_attribute = &p->m_attribute;
    }
    
    void visitSymName(SymName *p) {
      //WRITE ME

    }
    
    void visitPrimitive(Primitive *p) {
     // m_symboltable->insert(strdup(bt_to_string(p->m_parent_attribute->m_type.baseType)), &p->m_parent_attribute->m_type );
      //WRITE ME
      //->m_attribute.m_type.baseType = p->m_parent_attribute.m_type.baseType;
    }
    
    void visitClassName(ClassName *p) {
    
      //WRITE ME
    }
    
    
    void visitNullPointer() {}
};
