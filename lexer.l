%option yylineno
%pointer

%{
    #include <stdlib.h>
    #include <string.h>
    #include "ast.hpp"
    #include "primitive.hpp"
    #include "symtab.hpp"
    #include "classhierarchy.hpp"
    #include "parser.hpp"
    
    void yyerror(const char *);
%}

/* Put your definitions here, if you have any */

%%


[ \t\n] ;
\/\*([^\*]|\*[^\/])*\*\/             ;

0|[1-9][0-9]* { yylval.u_base_int = atoi(yytext); 
				return NUM; }

"true"            { yylval.u_base_int = 1; return TRUE; }
"false"           { yylval.u_base_int = 0; return FALSE; }
"print"           { return PRINT; }
"+"             { return PLUS;}
"<"              { return LESSTHAN;}
"<="            { return LESSEQUAL;}

"."             { return DOT;}
"return"          { return RETURN;}
"Nothing"         { return NOTHING; }
"Bool"			{ return BOOL;}
"Int"			{ return INT;}
":"           { return COLON;}
","           { return COMMA;}
";"       { return SEMICOLON;}
"("       { return OPENPARAN;}
")"      { return CLOSEPARAN;}
"+"            { return PLUS;}
"-"           { return MINUS;}
"/"          { return DIVIDE;}
"*"           { return TIMES;}
"{"            { return OPENBRACE;}
"}"          { return CLOSEBRACE;}
"and"             { return AND;}
"not"            { return NOT;}
"="          { return EQUALS;}
"from"            { return FROM;}
"if"              { return IF; }
"then"            { return THEN;}


[a-z_][A-Za-z0-9_]*/[ \t\n]*"="   {yylval.u_base_charptr = strdup(yytext); return ASS;}
[a-z_][A-Za-z0-9_]*  {yylval.u_base_charptr = strdup(yytext); return VARIABLENAME;}
[a-z_][A-Za-z0-9_]*/"("   {yylval.u_base_charptr = strdup(yytext); return METHODNAME;}
[A-Z][A-Za-z0-9_]*     {yylval.u_base_charptr = strdup(yytext); return CLASSNAME;}


.                         { yyerror("invalid character"); }

%%

int yywrap(void) {
    return 1;
}
